#!/bin/bash

RELEASE="smtp"
NAMESPACE="smtp"

# This will uninstall the old version (no update)
helm uninstall --timeout=0s $RELEASE -n $NAMESPACE
# This will download dependencies
( cd ./chart/ && helm dependency update )

# This will install or update an installtion
helm install $RELEASE ./chart -n $NAMESPACE --debug --timeout 3m -f .test.yaml
